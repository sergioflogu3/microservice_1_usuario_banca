package com.banco.bancaInter.repository;

import com.banco.bancaInter.domain.CuentaBancaria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaBancariaRepository extends JpaRepository<CuentaBancaria, Integer> {

}

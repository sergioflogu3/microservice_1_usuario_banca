package com.banco.bancaInter.controller;

import com.banco.bancaInter.dto.ClienteCreate;
import com.banco.bancaInter.dto.ClienteDto;
import com.banco.bancaInter.service.ClienteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
public class ClienteController {
    ClienteService clienteService;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping("/cliente")
    public List<ClienteDto> getClientes() {
        return clienteService.getClientes();
    }

    @PostMapping("/cliente")
    public ClienteDto createCliente(@RequestBody ClienteCreate clienteCreate) {
        return clienteService.createCliente(clienteCreate);
    }
}

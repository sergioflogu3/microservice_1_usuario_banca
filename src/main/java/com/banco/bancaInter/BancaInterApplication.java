package com.banco.bancaInter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancaInterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancaInterApplication.class, args);
	}

}

package com.banco.bancaInter.domain;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class CuentaBancaria {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(unique = true, name = "numero_unico_cuenta")
    private long numeroUnicoCuenta;
    @Column(unique = true, name = "numero_tarjeta_credito")
    String numeroTarjetaCredito;
    @Column(unique = true, name = "pin_numerico")
    private long pinNumerico;
    private String moneda;
    @Column(name = "fecha_apertura")
    private LocalDate fechaApertura;

    public Integer getId() {
        return id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public long getNumeroUnicoCuenta() {
        return numeroUnicoCuenta;
    }

    public String getNumeroTarjetaCredito() {
        return numeroTarjetaCredito;
    }

    public long getPinNumerico() {
        return pinNumerico;
    }

    public String getMoneda() {
        return moneda;
    }

    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public void setNumeroUnicoCuenta(long numeroUnicoCuenta) {
        this.numeroUnicoCuenta = numeroUnicoCuenta;
    }

    public void setNumeroTarjetaCredito(String numeroTarjetaCredito) {
        this.numeroTarjetaCredito = numeroTarjetaCredito;
    }

    public void setPinNumerico(long pinNumerico) {
        this.pinNumerico = pinNumerico;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }
}

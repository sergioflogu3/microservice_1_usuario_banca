package com.banco.bancaInter.service;

import com.banco.bancaInter.domain.Cliente;
import com.banco.bancaInter.domain.CuentaBancaria;
import com.banco.bancaInter.dto.ClienteCreate;
import com.banco.bancaInter.dto.ClienteDto;
import com.banco.bancaInter.repository.ClienteRepository;
import com.banco.bancaInter.repository.CuentaBancariaRepository;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;
    @Autowired
    CuentaBancariaRepository cuentaBancariaRepository;
    @Autowired
    EntityManager entityManager;


    public List<ClienteDto> getClientes() {
        List<Cliente> clientes = clienteRepository.findAll();
        List<ClienteDto> clientesDto = new ArrayList<ClienteDto>();
        for (Cliente cliente : clientes) {
            ClienteDto clienteDto = new ClienteDto();
            clienteDto.setId(cliente.getId());
            clienteDto.setNombre(cliente.getNombre());
            clienteDto.setTipoIdentificacion(cliente.getTipoIdentificacion());
            clienteDto.setNumeroIdentificacion(cliente.getNumeroIdentificacion());
            clienteDto.setUsername(cliente.getUsername());
            clientesDto.add(clienteDto);
        }
        return clientesDto;
    }

    public ClienteDto createCliente(ClienteCreate clienteCreate) {
        Cliente cliente = new Cliente();
        cliente.setNombre(clienteCreate.getNombre());
        cliente.setTipoIdentificacion(clienteCreate.getTipoIdentificacion());
        cliente.setNumeroIdentificacion(clienteCreate.getNumeroIdentificacion());
        cliente.setUsername(clienteCreate.getUsername());
        cliente.setPassword(clienteCreate.getPassword());
        cliente = this.clienteRepository.save(cliente);
        ClienteDto clienteDto = getClienteDto(cliente);
        setCuentaBancaria(cliente);
        return clienteDto;
    }

//    public ClienteDto updateCliente(ClienteCreate clienteCreate, Integer id) {
//        Cliente cliente = this.clienteRepository.findAllById(id);
//    }

    private void setCuentaBancaria(Cliente cliente) {
        CuentaBancaria cuentaBancaria = new CuentaBancaria();
        cuentaBancaria.setIdCliente(cliente.getId());
        cuentaBancaria.setNumeroUnicoCuenta(this.generarCodigoUnico(Long.parseLong("10000000000")));
        cuentaBancaria.setNumeroTarjetaCredito(this.generarNumeroTarjeta());
        cuentaBancaria.setPinNumerico(this.generarCodigoUnico(1000));
        cuentaBancaria.setMoneda("Bolivianos");
        cuentaBancaria.setFechaApertura(LocalDate.now());
        cuentaBancariaRepository.save(cuentaBancaria);
    }

    private static ClienteDto getClienteDto(Cliente cliente) {
        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setId(cliente.getId());
        clienteDto.setNombre(cliente.getNombre());
        clienteDto.setTipoIdentificacion(cliente.getTipoIdentificacion());
        clienteDto.setNumeroIdentificacion(cliente.getNumeroIdentificacion());
        clienteDto.setUsername(cliente.getUsername());
        clienteDto.setPassword(cliente.getPassword());
        return clienteDto;
    }

    private long generarCodigoUnico(long dimension) {
        long local = dimension + 1;
        return (long)(Math.random() * local);
    }


    private String generarNumeroTarjeta() {
        String[] prefijos = {"4", "51", "52", "53", "54", "55", "34", "37"};

        Random rand = new Random();
        String prefijo = prefijos[rand.nextInt(prefijos.length)];
        StringBuilder numeroTarjeta = new StringBuilder(prefijo);
        while (numeroTarjeta.length() < 16) {
            numeroTarjeta.append(rand.nextInt(10));
        }
        int suma = 0;
        boolean doble = false;
        for (int i = numeroTarjeta.length() - 1; i >= 0; i--) {
            int digito = Integer.parseInt(String.valueOf(numeroTarjeta.charAt(i)));
            if (doble) {
                digito *= 2;
                if (digito > 9) {
                    digito -= 9;
                }
            }
            suma += digito;
            doble = !doble;
        }
        int digitoVerificacion = (10 - (suma % 10)) % 10;
        numeroTarjeta.append(digitoVerificacion);
        return numeroTarjeta.toString();
    }
}
